from rest_framework import viewsets
from .models import Producto
from .serializers import ProductoSerializer
from cafeteriaclase.permission import IsRecepcionista


class ProductosViewSet(viewsets.ModelViewSet):
    queryset = Producto.objects.all()   
    serializer_class = ProductoSerializer
    permission_classes = [IsRecepcionista] #Instancia y retorna la listaa de permisos que esta vista requiere
   
