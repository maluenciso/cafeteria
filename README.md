# Cafeteria
video del funcionamiento del sistema

https://www.loom.com/share/5d50b43285744ed9afa9578a5cbb5e7f

Para que funcione el proyecto abrir el cuadro de dialogo y ejecutar el siguiente comando:
 chrome.exe --user-data-dir="C:/Chrome dev session" --disable-web-security

de lo contrario el api/token no funciona

## Proceso para que funcione el back
El grupo esta conformado por María Luz Enciso, Nilsa Iriarte y Antonio Benitez.

Para instalar el proyecto

1 Clona el proyecto
2 Crea un entorno virtual con el siguiente comando 

```
python -m venv venv
```
3 Activar el entorno

```
 .\venv\Scripts\activate
```

4 Instalar requerimientos

```
pip install -r requirements.txt
```

5  Migrar a la BD
```
python  .\manage.py migrate

```
6 crear el super user

```
python  .\manage.py createsuperuser
```
7 correr el proyecto

```
python  .\manage.py runserver
```


## Proceso para que funcione el front

1 Ejecutar comando 
```
npm install
```

2 Hacer correr el proyecto

```
npm run dev
```